  Feature: Add language toggle in the document view to change the document display language. 
  As a user I want to be able to change document display language directly in the document
  view when I am reviewing publications so that I can compare and cross-reference different
  languages of the same publication in an efficient way. 
  
  Scenario Outline: The user hasn't selected an option from the dropdown.
  The user can see a dropdown menu with the available languages in the publication view
    Given the user is on the publication view page
      And the publication has the following <available languages>
      And the publication language is <publication language>
      And the selected language in the user preferences is <preference language>
     When the user clicks on the dropdown button
     Then the user should be able to see the following <labels> in the dropdown menu
      And the selected language is <selected language>
      And the section labels are not activated
    Examples: 
      | available languages | publication language | labels                                                            | preference language | selected language | 
      | "eng, chi, jpn"     | English              | "Language of publication, Chinese (preference setting), Japanese" | Chinese             | Chinese           | 
      | "eng, chi, jpn"     | English              | "Language of publication (preference setting), Chinese, Japanese" | English             | English           | 
      | "eng, chi, jpn"     | German               | "English (preference setting), Chinese, Japanese"                 | English             | English           | 
      | "eng, chi, jpn"     | German               | "English, Chinese, Japanese"                                      | German              | ""                | 
  
  Scenario Outline: The user has selected an option from the dropdown. 
  The user can see the selected language. The user's selection is persisted
    Given the user is on the publication view page
      And the publication has the following <available languages>
      And the selected language in the user preferences is <preference language>
      And the user selects a <language> from the dropdown menu
      And the user reloads the publication view
     When the user clicks on the dropdown button
     Then the selected language is <selected language>
      And the section labels are activated
    Examples: 
      | available languages | language | preference language | selected language | 
      | "eng, chi, jpn"     | English  | Chinese             | English           | 
      | "eng, chi, jpn"     | English  | English             | English           | 
      | "eng, chi, jpn"     | Chinese  | German              | Chinese           | 
  
  Scenario Outline: The user opens a publication view on a publication view popup
    Given the user is on the publication view page
      And the publication has the following <available languages>
      And the user selects a <language> from the dropdown menu
      And the user navigates to the section
     When the user clicks a publication view link
     Then the publication view popup opens
      And the publication language is <popup publication language>
    Examples: 
      | available languages | language | popup publication language | 
      | "eng, chi, jpn"     | English  | English                    | 